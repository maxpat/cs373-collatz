# CS373: Software Engineering Collatz Repo

* Name: (your Full Name)
	Max Patrick
* EID: (your EID)
	mep3343
* GitLab ID: (your GitLab ID)
	maxpat
* HackerRank ID: (your HackerRank ID)
	maxpat21
* Git SHA: (most recent Git SHA, final change to your repo will be adding this value)
	12aa52403036dc81ed0574856969ff6fa9e3a4e0
* GitLab Pipelines: (link to your GitLab CI Pipeline)
	https://gitlab.com/maxpat/cs373-collatz/pipelines
* Estimated completion time: (estimated time in hours, int or float)
	5 Hours
* Actual completion time: (actual time in hours, int or float)
	6 Hours
* Comments: (any additional comments you have)
	Already knew what approach I wanted to use for my solution from having
	solved this problem for OOP last semester, just struggled with some of
	the nuances of python.
